package com.jonmort.stash.hipchat;

import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;
import org.apache.commons.lang.StringUtils;

import java.util.Map;

public class HipChatConfiguredCondition implements Condition {
    private final RoomNotificationManager roomNotificationManager;

    public HipChatConfiguredCondition(RoomNotificationManager roomNotificationManager) {
        this.roomNotificationManager = roomNotificationManager;
    }

    @Override
    public void init(Map<String, String> stringStringMap) throws PluginParseException {
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> stringObjectMap) {
        return StringUtils.isNotBlank(roomNotificationManager.getApiKey());
    }
}
