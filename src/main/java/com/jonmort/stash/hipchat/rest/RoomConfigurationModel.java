package com.jonmort.stash.hipchat.rest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class RoomConfigurationModel {

    @JsonProperty
    private Integer room_id;

    @JsonProperty
    private String projectKey;

    public RoomConfigurationModel() {
    }

    public RoomConfigurationModel(String projectKey, Integer room_id) {
        this.projectKey = projectKey;
        this.room_id = room_id;
    }

    public String getProjectKey() {
        return projectKey;
    }

    public void setProjectKey(String projectKey) {
        this.projectKey = projectKey;
    }

    public Integer getRoom_id() {
        return room_id;
    }

    public void setRoom_id(Integer room_id) {
        this.room_id = room_id;
    }
}
