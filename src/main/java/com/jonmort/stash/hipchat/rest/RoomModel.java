package com.jonmort.stash.hipchat.rest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class RoomModel {

    @JsonProperty
    private int room_id;
    @JsonProperty
    private String name;
    @JsonProperty
    private String topic;
    @JsonProperty
    private long last_active;
    @JsonProperty
    private long created;
    @JsonProperty
    private String owner_user_id;
    @JsonProperty
    private boolean is_archived;
    @JsonProperty
    private boolean is_private;
    @JsonProperty
    private String xmpp_jid;

    public long getCreated() {
        return created;
    }

    public void setCreated(long created) {
        this.created = created;
    }

    public boolean isIs_archived() {
        return is_archived;
    }

    public void setIs_archived(boolean is_archived) {
        this.is_archived = is_archived;
    }

    public boolean isIs_private() {
        return is_private;
    }

    public void setIs_private(boolean is_private) {
        this.is_private = is_private;
    }

    public long getLast_active() {
        return last_active;
    }

    public void setLast_active(long last_active) {
        this.last_active = last_active;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner_user_id() {
        return owner_user_id;
    }

    public void setOwner_user_id(String owner_user_id) {
        this.owner_user_id = owner_user_id;
    }

    public int getRoom_id() {
        return room_id;
    }

    public void setRoom_id(int room_id) {
        this.room_id = room_id;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getXmpp_jid() {
        return xmpp_jid;
    }

    public void setXmpp_jid(String xmpp_jid) {
        this.xmpp_jid = xmpp_jid;
    }
}
