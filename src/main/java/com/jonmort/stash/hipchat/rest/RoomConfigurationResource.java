package com.jonmort.stash.hipchat.rest;

import com.jonmort.stash.hipchat.RoomNotificationManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/room/{project-key}")
@Produces(MediaType.APPLICATION_JSON)
public class RoomConfigurationResource {

    private final RoomNotificationManager roomNotificationManager;

    public RoomConfigurationResource(RoomNotificationManager roomNotificationManager) {
        this.roomNotificationManager = roomNotificationManager;
    }

    @GET
    public Response getRoom(@PathParam("project-key") String projectKey) {
        final Integer roomForProjectId = roomNotificationManager.getRoomForProjectKey(projectKey);
        final RoomConfigurationModel room = new RoomConfigurationModel(projectKey, roomForProjectId);
        return Response.ok(room).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public Response setRoom(@PathParam("project-key") String projectKey, @FormParam("room") final int room) {
        roomNotificationManager.setRoomForProjectKey(projectKey, room);
        final RoomConfigurationModel model = new RoomConfigurationModel(projectKey, room);
        return Response.ok(model).build();
    }
}
