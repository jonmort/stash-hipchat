package com.jonmort.stash.hipchat.rest;

import com.atlassian.sal.api.net.Response;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.net.ReturningResponseHandler;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize
public class RoomsModel {
    @JsonProperty
    private List<RoomModel> rooms;

    public List<RoomModel> getRooms() {
        return rooms;
    }

    public void setRooms(List<RoomModel> rooms) {
        this.rooms = rooms;
    }

    public static ReturningResponseHandler<Response, RoomsModel> responseHandler() {
        return new RoomsModelResponseHandler();
    }

    private static class RoomsModelResponseHandler implements ReturningResponseHandler<Response, RoomsModel> {

        @Override
        public RoomsModel handle(Response response) throws ResponseException {
            return response.getEntity(RoomsModel.class);
        }
    }
}
