package com.jonmort.stash.hipchat.rest;

import com.jonmort.stash.hipchat.RoomNotificationManager;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/api")

@Produces({MediaType.APPLICATION_JSON})
public class ApiConfigurationResource {
    private final RoomNotificationManager roomNotificationManager;

    public ApiConfigurationResource(RoomNotificationManager roomNotificationManager) {
        this.roomNotificationManager = roomNotificationManager;
    }

    @GET
    public Response getSettings() {
        final ApiConfigurationModel apiModel = new ApiConfigurationModel(roomNotificationManager.getApiKey(), roomNotificationManager.getHipchatUsername());
        return Response.ok(apiModel).build();
    }

    @POST
    @Consumes({MediaType.APPLICATION_FORM_URLENCODED})
    public Response setSettings(@FormParam("apikey") final String apiKey, @FormParam("username") final String username) {
        roomNotificationManager.setApiKey(apiKey);
        roomNotificationManager.setHipchatUsername(username);
        return Response.ok(new ApiConfigurationModel(apiKey, username)).build();
    }
}
