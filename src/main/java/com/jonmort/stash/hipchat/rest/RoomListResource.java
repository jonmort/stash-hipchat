package com.jonmort.stash.hipchat.rest;

import com.atlassian.sal.api.net.*;
import com.jonmort.stash.hipchat.HipChatUriBuilder;
import com.jonmort.stash.hipchat.RoomNotificationManager;
import org.apache.commons.lang.StringUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rooms")
@Produces(MediaType.APPLICATION_JSON)
public class RoomListResource {

    private final RoomNotificationManager roomNotificationManager;
    private final RequestFactory requestFactory;

    public RoomListResource(RoomNotificationManager roomNotificationManager, RequestFactory requestFactory) {
        this.roomNotificationManager = roomNotificationManager;
        this.requestFactory = requestFactory;
    }

    @GET
    public Response getRooms() {
        final String apiKey = roomNotificationManager.getApiKey();
        if (StringUtils.isBlank(apiKey)) {
            return Response.noContent().build();
        }
        final Request request = requestFactory.createRequest(Request.MethodType.GET, HipChatUriBuilder.roomsList(apiKey).toASCIIString());
        try {
            RoomsModel roomsModel = (RoomsModel) request.executeAndReturn(RoomsModel.responseHandler());
            return Response.ok(roomsModel).build();
        } catch (ResponseException e) {
            return Response.serverError().entity(e).build();
        }
    }

}
