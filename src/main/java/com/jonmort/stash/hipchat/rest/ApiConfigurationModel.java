package com.jonmort.stash.hipchat.rest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize
public class ApiConfigurationModel {

    @JsonProperty
    private String apiKey;

    @JsonProperty
    private String username;

    public ApiConfigurationModel(String apiKey, String username) {
        this.apiKey = apiKey;
        this.username = username;
    }

    public ApiConfigurationModel() {
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
