package com.jonmort.stash.hipchat;

import javax.ws.rs.core.UriBuilder;
import java.net.URI;

public class HipChatUriBuilder {

    public static URI roomsList(String authToken) {
        return hipchatUri().path("rooms").path("list").queryParam("format", "json").queryParam("auth_token", authToken).build();
    }

    public static URI roomsMessage(String authToken) {
        return hipchatUri().path("rooms").path("message").queryParam("format", "json").queryParam("auth_token", authToken).build();
    }

    private static UriBuilder hipchatUri() {
        return UriBuilder.fromUri(System.getProperty("hipchat.url", "https://api.hipchat.com/v1/"));
    }
}
