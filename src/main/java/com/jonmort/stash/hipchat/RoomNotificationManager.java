package com.jonmort.stash.hipchat;

import com.atlassian.event.api.EventListener;
import com.atlassian.sal.api.net.Request;
import com.atlassian.sal.api.net.RequestFactory;
import com.atlassian.sal.api.net.ResponseException;
import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.soy.renderer.SoyException;
import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.event.RepositoryPushEvent;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.project.Project;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.server.ApplicationPropertiesService;
import com.google.common.collect.ImmutableMap;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class RoomNotificationManager {
    public static final String HIPCHAT_USERNAME_KEY = "hipchat-username";
    private final Logger log = LoggerFactory.getLogger(RoomNotificationManager.class);

    public static final String SETTINGS_KEY = "com.jonmort.stash.stash-hipcaht";
    public static final String API_KEY_KEY = "api-key";
    public static final String PROJECT_MAP_KEY = "project-map";
    private final PluginSettings pluginSettings;
    private final RequestFactory requestFactory;
    private final SoyTemplateRenderer soyTemplateRenderer;
    private final NavBuilder navBuilder;


    public RoomNotificationManager(final PluginSettingsFactory pluginSettingsFactory, RequestFactory requestFactory, SoyTemplateRenderer soyTemplateRenderer, NavBuilder navBuilder) {
        this.requestFactory = requestFactory;
        this.soyTemplateRenderer = soyTemplateRenderer;
        this.navBuilder = navBuilder;
        pluginSettings = pluginSettingsFactory.createSettingsForKey(SETTINGS_KEY);
    }

    public String getApiKey() {
        final Object apiKey = pluginSettings.get(API_KEY_KEY);
        if (apiKey instanceof String)
            return (String) apiKey;
        else return "";
    }

    public void setApiKey(String apiKey) {
        if (StringUtils.isBlank(apiKey)) pluginSettings.remove(API_KEY_KEY);
        else pluginSettings.put(API_KEY_KEY, apiKey);
    }

    public String getHipchatUsername() {
        final Object apiKey = pluginSettings.get(HIPCHAT_USERNAME_KEY);
        if (apiKey instanceof String)
            return (String) apiKey;
        else return "";
    }

    public void setHipchatUsername(String username) {
        if (StringUtils.isBlank(username)) pluginSettings.remove(HIPCHAT_USERNAME_KEY);
        else pluginSettings.put(HIPCHAT_USERNAME_KEY, username);
    }

    public Integer getRoomForProject(Project project) {
        return getRoomForProjectKey(project.getKey());
    }

    public Integer getRoomForProjectKey(String projectKey) {
        final String room = getProjectKeyToRoomMap().get(projectKey);
        if (room == null) {
            return null;
        }
        return Integer.parseInt(room);
    }

    public void setRoomForProject(Project project, int room) {
        final String projectKey = project.getKey();
        setRoomForProjectKey(projectKey, room);
    }

    public void setRoomForProjectKey(String projectKey, int room) {
        final Map<String, String> projectIdToRoomMap = getProjectKeyToRoomMap();
        projectIdToRoomMap.put(projectKey, Integer.toString(room));
        storeProjectKeyToRoomMap(projectIdToRoomMap);
    }

    private void storeProjectKeyToRoomMap(Map<String, String> projectKeyToRoomMap) {
        pluginSettings.put(PROJECT_MAP_KEY, projectKeyToRoomMap);
    }

    private Map<String, String> getProjectKeyToRoomMap() {
        final Object o = pluginSettings.get("project-map");
        if (o instanceof Map) {
            return (Map<String, String>) o;
        } else {
            Map<String, String> roomMap = new HashMap<String, String>();
            pluginSettings.put("project-map", roomMap);
            return roomMap;
        }
    }

    @EventListener
    public void pushEvent(RepositoryPushEvent pushEvent) throws UnsupportedEncodingException {
        final Project project = pushEvent.getRepository().getProject();
        final Integer room = getRoomForProject(project);
        if (room != null) {
            final Request request = requestFactory.createRequest(Request.MethodType.POST, HipChatUriBuilder.roomsMessage(getApiKey()).toASCIIString());
            request.setRequestContentType("application/x-www-form-urlencoded");
            final String hipchatUsername = StringUtils.defaultIfBlank(getHipchatUsername(), "Stash");
            request.addRequestParameters("room_id", Integer.toString(room),
                    "from", hipchatUsername,
                    "message", generateMessage(pushEvent));
            try {
                request.execute();
            } catch (ResponseException e) {
                log.error("Couldn't post message to HipChat", e);
            }
        }

    }

    private String generateMessage(RepositoryPushEvent pushEvent) {
        final Repository repository = pushEvent.getRepository();
        final Project project = repository.getProject();

        try {
            return soyTemplateRenderer.render("com.jonmort.stash.hipchat.stash-hipchat:hipchat-message-soy", "hipchat.stash.message.message",
                    ImmutableMap.<String, Object>of(
                            "user", pushEvent.getUser().getDisplayName(),
                            "project", project,
                            "projectUrl", navBuilder.project(project).buildAbsolute(),
                            "repository", repository,
                            "repositoryUrl", navBuilder.repo(repository).commits().buildAbsolute()));
        } catch (SoyException e) {
            return e.getMessage();
        }
    }
}
