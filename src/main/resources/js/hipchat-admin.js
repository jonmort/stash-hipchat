AJS.$(document).ready(function ($) {
    var dialog = new AJS.Dialog({id:"hipchat-config-dialog", width:450, height:250, closeOnOutsideClick:true});

    function save(dialog) {
        var form = $('#' + dialog.id).find("form");
        $.ajax({
            url:form.attr('action'),
            type:'POST',
            data:form.serialize(),
            dataType:'json',
            success:function (data) {
                AJS.messages.success("#hipchat-stash-aui-message-bar", {
                    title:"HipChat Config Saved Successfully"
                });
            },
            complete:function (data) {
                dialog.hide();
            },
            error:function (jqXHR, textStatus, errorThrown) {
                AJS.messages.error("#hipchat-stash-aui-message-bar", {
                    title:"Could not save HipChat Config",
                    body:textStatus
                });
            }
        })
    }

    function cancel(dialog) {
        dialog.hide();
    }

    dialog.addButton("Save", save);
    dialog.addButton("Cancel", cancel);

    var initAndShow = function () {
        $.ajax({
            url:AJS.contextPath() + "/rest/stash-hipchat/1.0/api",
            type:'GET',
            dataType:'json',
            success:function (data) {
                data.context = AJS.contextPath();
                dialog.addPanel("HipChat Config", hipchat.stash.dialog(data));
                dialog.show();
            },
            error:function (jqXHR, textStatus, errorThrown) {
                AJS.messages.error("#hipchat-stash-aui-message-bar", {
                    title:"Could not show HipChat Config",
                    body:textStatus
                });
            }
        });
        initAndShow = function () {
            dialog.show();
        };
    };


    $("a.hipchat-config-link").on("click", function (e) {
        e.preventDefault();
        initAndShow();
    });

});
