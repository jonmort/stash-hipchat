AJS.$(document).ready(function ($) {

    function save(dialog) {
        $.ajax({
            url:AJS.contextPath() + "/rest/stash-hipchat/1.0/room/" + $("#content").data("projectkey"),
            type:'POST',
            data:$(dialog).find("form").serialize(),
            dataType:'json',
            success:function (data) {
                AJS.messages.success("#hipchat-stash-aui-message-bar", {
                    title:"HipChat Config Saved Successfully"
                });
            },
            complete:function (data) {
                dialog.hide();
            },
            error:function (jqXHR, textStatus, errorThrown) {
                AJS.messages.error("#hipchat-stash-aui-message-bar", {
                    title:"Could not save HipChat Config",
                    body:textStatus
                });
            }
        })
    }

    var initAndShow = function (contents, showPopup) {
        $.when(
            $.ajax({
                url:AJS.contextPath() + "/rest/stash-hipchat/1.0/rooms",
                type:'GET',
                dataType:'json'
            }),
            $.ajax({
                url:AJS.contextPath() + "/rest/stash-hipchat/1.0/room/" + $("#content").data("projectkey"),
                type:'GET',
                dataType:'json'
            })
        ).then(function (roomsReq, configReq) {
                var rooms = roomsReq[0];
                rooms.selected = configReq[0].room_id;
                $(contents).append(hipchat.stash.rooms.form(rooms));
                showPopup();
            },
            function () {
                AJS.messages.error("#hipchat-stash-aui-message-bar", {
                    title:"Could not show HipChat Config"
                });
            });
        initAndShow = function () {
            showPopup();
        };

    };


    var dialog = AJS.InlineDialog($("a.hipchat-repo-config-link"), "hipchat-room-config-dialog", function (contents, trigger, showPopup) {
        initAndShow(contents, showPopup);
    });

    $(document).on("change", "#hipchat-room", function (e) {
        save(dialog);
        dialog.hide();
    });

});
