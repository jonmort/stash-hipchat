package testplugin;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.stash.page.RepositoryEditSettingsPage;
import com.atlassian.webdriver.stash.page.StashPage;

import java.util.List;

public class HipChatStashRepoConfigDialogPage extends RepositoryEditSettingsPage {
    @ElementBy(id = "hipchat-room")
    private PageElement roomList;

    public HipChatStashRepoConfigDialogPage(String projectKey, String slug) {
        super(projectKey, slug);
    }

}
