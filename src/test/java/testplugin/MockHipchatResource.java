package testplugin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rooms")
@Produces(MediaType.APPLICATION_JSON)
public class MockHipchatResource {

    @GET
    @Path("/list")
    public Response rooms(@QueryParam("format") final String format, @QueryParam("auth_token") final String authToken) {
        if (format.equals("json") && authToken != null)

            // from https://www.hipchat.com/docs/api/method/rooms/list
            return Response.ok("{\n" +
                    "  \"rooms\": [\n" +
                    "    {\n" +
                    "      \"room_id\": 7,\n" +
                    "      \"name\": \"Development\",\n" +
                    "      \"topic\": \"Make sure to document your API functions well!\",\n" +
                    "      \"last_active\": 1269020400,\n" +
                    "      \"created\": 1269010311,\n" +
                    "      \"owner_user_id\": 1,\n" +
                    "      \"is_archived\": false,\n" +
                    "      \"is_private\": false,\n" +
                    "      \"xmpp_jid\": \"7_development@conf.hipchat.com\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"room_id\": 10,\n" +
                    "      \"name\": \"Ops\",\n" +
                    "      \"topic\": \"Chef is so awesome.\",\n" +
                    "      \"last_active\": 1269010500,\n" +
                    "      \"created\": 1269010211,\n" +
                    "      \"owner_user_id\": 5,\n" +
                    "      \"is_archived\": false,\n" +
                    "      \"is_private\": true,\n" +
                    "      \"xmpp_jid\": \"10_ops@conf.hipchat.com\"\n" +
                    "    }\n" +
                    "  ]\n" +
                    "}" +
                    "}", MediaType.APPLICATION_JSON_TYPE).build();
        else
            return Response.status(Response.Status.BAD_REQUEST).build();
    }

    @POST
    @Path("/message")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response message(@FormParam("message") final String message,
                            @FormParam("room_id") final String roomId,
                            @FormParam("from") final String from,
                            @QueryParam("format") final String format,
                            @QueryParam("auth_token") final String authToken
    ) {
        if (message != null && roomId != null && from != null && format.equals("json") && authToken != null)
            // from https://www.hipchat.com/docs/api/method/rooms/message
            return Response.ok("{\n" +
                    "  \"status\": \"sent\"\n" +
                    "}", MediaType.APPLICATION_JSON).build();
        else
            return Response.status(Response.Status.BAD_REQUEST).build();

    }
}
