package testplugin;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.webdriver.stash.page.RepositoryEditSettingsPage;

public class HipChatStashRepoConfigPage extends RepositoryEditSettingsPage {
     @ElementBy(className = "hipchat-repo-config-link")
    private PageElement hipchatConfig;
    public HipChatStashRepoConfigPage(String projectKey, String slug) {
        super(projectKey, slug);
    }

    public HipChatStashRepoConfigDialogPage clickHipchatConfigButton() {
        hipchatConfig.click();
        return pageBinder.bind(HipChatStashRepoConfigDialogPage.class, projectKey, slug);
    }
}
